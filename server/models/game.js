const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const gameSchema = new Schema({
	gameState: [[Schema.Types.Mixed]],
	gameMove: Number,
	size: Number,
	xNext: Boolean,
	winner: String,
	highlight: [[Boolean]],
	history: [{
		gameState: [[Schema.Types.Mixed]],
		gameMove: Number
	}]
});

module.exports = gameSchema;