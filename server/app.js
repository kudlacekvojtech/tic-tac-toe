const color = require('colors');
const express = require('express');
const app = express();
const { GraphQLServer } = require('graphql-yoga');
const { Prisma } = require('prisma-binding');

const port = {
	default: 3000,
	graphql: 4000
};

//! App server

app.use(express.static('../build'));

app.get('*', (req, res) => {
	res.sendFile('index.html', { root: __dirname + '/../build' });
});

app.listen(port.default, () => console.log(`App is running on port ${color.blue(port.default)}`));

//! GraphQL server

const resolvers = {
	Query: {
		game: (_, args, context, info) => {
			return context.prisma.query.game(args, info);
		},
		games: (_, args, context, info) => {
			return context.prisma.query.games(args, info);
		}
	},
	Mutation: {
		createGame: (_, args, context, info) => {
			return context.prisma.mutation.createGame(args, info)
		}
	}
}

const server = new GraphQLServer({
	//typeDefs: './schema/schema.graphql',
	typeDefs: './generated/prisma.graphql',
	resolvers,
	resolverValidationOptions: {
		requireResolversForResolveType: false
	},
	context: req => ({
		...req,
		prisma: new Prisma({
			typeDefs: './generated/prisma.graphql',
			endpoint: 'https://eu1.prisma.sh/vojtech-kudlacek/prisma/dev',
		}),
	}),
})
server.start({ port: port.graphql }, () => console.log(`GraphQL server is running on port ${color.blue(port.graphql)}`))