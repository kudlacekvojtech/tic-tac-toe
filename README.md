# Tic-tac-toe

This is project is just a sample how I learned some technologies like React, GraphQL, Apollo, Prisma, etc. 

## Getting Started

Before cloning the project remember that it is only my "learn to" project, where I've learned things like React, apollo, graphql, prisma, etc.

### Installing

Clone the repository.

```
git clone https://bitbucket.org/kudlacekvojtech/tic-tac-toe.git
```

Open terminal, go to the project folder and install all packages and dependencies by running:

```
npm i
```

Now, you should be ready to go.

## Deployment

There are 2 applications that you want to run at the same time.

Best way to do that is building the frontend app with:

```
npm run build
```

Then just run the server by:

```
cd server; node app
```

For result just visit `localhost:3000`

## Authors

* **Vojtěch Kudláček** - *Initial work* - [VojtechKudlacek](https://bitbucket.org/kudlacekvojtech)

## Acknowledgments

* I was inspired by [tutorial](https://reactjs.org/tutorial/tutorial.html) to React
* This project is just a sample how I learned these technologies, nothing else.