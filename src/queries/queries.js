import { gql } from 'apollo-boost';

//! Local

const localGameQuery = gql`
	query {
		localGame @client {
			gameState
			gameMove
			size
			xNext
			winner
			highlight
			history
		}
	}
`;

const updateLocalGameMutation = gql`
	mutation($data: GameCreateInput!) {
		updateLocalGame(data: $data) @client {
			gameState
			gameMove
			size
			xNext
			winner
			highlight
			history
		}
	}
`;

//! Network

const gameQuery = gql`
	query($where: GameWhereUniqueInput!) {
		game(where: $where) {
			gameState
			gameMove
			size
			xNext
			winner
			highlight
			history
		}
	}
`;

const gamesQuery = gql`
	query {
		games(first: 8, orderBy: createdAt_DESC) {
			id
			size
			winner
			gameMove
		}
	}
`;

const createGameMutation = gql`
	mutation($data: GameCreateInput!){
		createGame(data: $data) {
			gameState
			gameMove
			size
			xNext
			winner
			highlight
			history
		}
	}
`;

export {
	localGameQuery,
	updateLocalGameMutation,
	gameQuery,
	gamesQuery,
	createGameMutation
};