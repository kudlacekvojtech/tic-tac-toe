export class GameManager {

	static createGame(size, fill = null) {
		let arr = Array(size).fill(fill);
		for (let i = 0; i < arr.length; i++)
			arr[i] = Array(size).fill(fill);
		return arr;
	}

	static duplicate(what) {
		return JSON.parse(JSON.stringify(what));
	}

	static checkWinner(tiles, size, x, y) {
		/**
		 * 3		-	3
		 * 4-7		-	4
		 * 8-20		-	5 
		 */
		const needToWin = (size === 3) ? 3 : (size < 8) ? 4 : 5;
		const player = tiles[x][y];
		let checkState = 0;
		let result = {
			win: false,
			highlight: []
		};
		while (checkState < 4) {
			let has = 0;
			let i = 0;
			let j = 0;
			let highlight = this.createGame(size);
			switch (checkState) {
				case 0: // Check right ✓
					i = y;
					while (tiles[x][i - 1] && tiles[x][i - 1] === player) { i--; }
					for (null; (i < size); i++) {
						if (!tiles[x][i] || tiles[x][i] !== player)
							break;
						has++;
						highlight[x][i] = true;
					}
					break;
				case 1: // Check down ✓
					i = x;
					while (tiles[i - 1] && tiles[i - 1][y] === player) { i--; }
					for (null; (i < size); i++) {
						if (!tiles[i][y] || tiles[i][y] !== player)
							break;
						has++;
						highlight[i][y] = true;
					}
					break;
				case 2: // Check diagonal right down ✓
					i = x;
					j = y;
					while (tiles[i - 1] && tiles[i - 1][j - 1] && tiles[i - 1][j - 1] === player) { i--; j--; }
					for (null; (i < size) && (j < size); i++ , j++) {
						if (!tiles[i][j] || tiles[i][j] !== player)
							break;
						has++;
						highlight[i][j] = true;
					}
					break;
				case 3: // Check diagonal left down ✓
					i = x;
					j = y;
					while (tiles[i - 1] && tiles[i - 1][j + 1] && tiles[i - 1][j + 1] === player) { i--; j++; }
					for (null; (i < size) && (j >= 0); i++ , j--) {
						if (!tiles[i][j] || tiles[i][j] !== player)
							break;
						has++;
						highlight[i][j] = true;
					}
					break;
				default:
					break;
			}
			if (has >= needToWin) {
				result.win = true;
				result.highlight = highlight;
				break;
			}
			checkState++;
		}
		return result;
	}

}