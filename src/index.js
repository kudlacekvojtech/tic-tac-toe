//! Core
import React from 'react';
import ReactDOM from 'react-dom';
//! Styles
import './index.css';
//! App
import App from './app';

ReactDOM.render(
	<App/>,
	document.getElementById('root')
);