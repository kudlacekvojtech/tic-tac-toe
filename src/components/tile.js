import React from 'react';

const Tile = (props) => {
	return (
		<button className={`tile ${props.value ? 'disabled' : ''} ${props.highlight ? 'highlight' : ''}`} onClick={props.onClick}>
			{props.value}
		</button>
	);
}

export default Tile; 