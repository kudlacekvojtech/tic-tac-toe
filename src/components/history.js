import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';
import { gameQuery, gamesQuery } from '../queries/queries';

class History extends Component {

	selectGame(id) {
		this.props.gameQuery.refetch({ where: { id } }).then(res => {
			this.props.load(res.data.game);
		});
	}

	getHistory() {
		if (!this.props.gamesQuery.loading && this.props.gamesQuery.games) {
			let { games } = this.props.gamesQuery;
			let list = [];
			for (let game of games) {
				list.push(
					<div
						key={game.id}
						onClick={() => this.selectGame(game.id)}
						className="historyRow"
					>
						<span>ID: <b>{game.id}</b></span>
						<span>Size: <b>{game.size}x{game.size}</b></span>
						<span>Winner: <b>{game.winner}</b></span>
						<span>Moves: <b>{game.gameMove}</b></span>
					</div>
				)
			}
			return list;
		}
	}

	render() {
		return (
			<div className="history">
				{this.getHistory()}
			</div>
		)
	}

}

export default compose(
	graphql(gamesQuery, { name: 'gamesQuery' }),
	graphql(gameQuery, { name: 'gameQuery', options: { variables: { where: { id: '' } } } })
)(History);