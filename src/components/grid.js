import React, { Component } from 'react';
import Tile from './tile';

class Grid extends Component {

	createTile(x, y) {
		return (
			<Tile
				key={`tile-${x}-${y}`}
				value={this.props.tiles[x][y]}
				highlight={this.props.highlight[x][y]}
				onClick={() => this.props.onClick(x, y)}
			/>
		);
	}

	createGrid(size) {
		let rows = [];
		for (let x = 0; x < size; x++) {
			let tiles = [];
			for (let y = 0; y < size; y++)
				tiles.push(this.createTile(x, y));
			rows.push(<div key={`row-${x}`} className="gridRow">{tiles}</div>);
		}
		return rows;
	}

	render() {
		return (
			<div className="grid">
				{this.createGrid(this.props.size)}
			</div>
		);
	}

}

export default Grid;