import React, { Component } from 'react';

class Settings extends Component {

	constructor(props) {
		super(props);
		this.state = { value: 0 };
	}

	render() {
		return (
			<div className="settings">
				<input className="settingsInput" placeholder="Set grid size [3-20]" onChange={(e) => this.setState({ value: e.target.value })} />
				<button className="settingsButton" onClick={() => this.props.onClick(parseInt(this.state.value, 10))}>Change size</button>
			</div>
		);
	}

}

export default Settings;