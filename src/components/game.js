//! React
import React, { Component } from 'react';
//! Libs
import { graphql, compose } from 'react-apollo';
//! Components
import Grid from './grid';
import Settings from './settings';
import History from './history';
//! Services
import { GameManager } from '../services/gameManager';
//! Queries
import { localGameQuery, updateLocalGameMutation, createGameMutation, gamesQuery } from '../queries/queries';

class Game extends Component {

	/**
	 * Update cache
	 */
	updateState(data) {
		return this.props.updateLocalGameMutation({ variables: { data } });
	}

	/**
	 * Return current game state
	 */
	getLocalGame() {
		return this.props.localGameQuery.localGame;
	}

	/**
	 * Check winner from the last clicked tile 
	 */
	async checkWinner(x, y) {
		const localGame = this.getLocalGame();
		const player = localGame.gameState[x][y];
		const result = GameManager.checkWinner(localGame.gameState.slice(), localGame.size, x, y);
		if (result.win) {
			const res = await this.updateState({
				winner: player,
				highlight: result.highlight
			});
			this.saveResults(res.data.updateLocalGame);
		}
	}

	/**
	 * Update database with new game
	 */
	saveResults(state) {
		this.props.createGameMutation({
			variables: {
				data: {
					gameState: JSON.stringify(state.gameState),
					gameMove: state.gameMove,
					size: state.size,
					xNext: state.xNext,
					winner: state.winner,
					highlight: JSON.stringify(state.highlight),
					history: JSON.stringify(state.history)
				}
			},
			refetchQueries: [{ query: gamesQuery }]
		});
	}

	/**
	 * Add last move to the history
	 */
	async updateHistory() {
		const localGame = this.getLocalGame();
		const history = GameManager.duplicate(localGame.history);
		history.push(GameManager.duplicate(localGame.gameState));
		return await this.updateState({ history });
	}

	/**
	 * Update clicked tile and game state
	 */
	async updateCurrent(x, y) {
		const localGame = this.getLocalGame();
		let tiles = GameManager.duplicate(localGame.gameState);
		tiles[x][y] = localGame.xNext ? 'X' : 'O';
		return await this.updateState({
			gameState: tiles,
			gameMove: (localGame.gameMove + 1),
			xNext: !localGame.xNext,
			history: localGame.history
		});
	}

	/**
	 * React to tile click
	 */
	async handleClick(x, y) {
		const localGame = this.getLocalGame();
		if (localGame.winner || localGame.gameState[x][y])
			return;
		await this.updateHistory();
		await this.updateCurrent(x, y);
		await this.checkWinner(x, y);
		return;
	}

	/**
	 * Go back to move number `x`
	 */
	backInTime(move) {
		const localGame = this.getLocalGame();
		this.updateState({
			gameState: localGame.history[move],
			gameMove: move,
			xNext: (move % 2) === 0,
			winner: null,
			highlight: GameManager.createGame(localGame.size),
			history: localGame.history.slice(0, move)
		});
	}

	/**
	 * Get actual game state
	 */
	getStatus() {
		const localGame = this.getLocalGame();
		const winner = localGame.xNext ? 'X' : 'O';
		if (localGame.winner)
			return (<span>{winner} wins!</span>);
		else
			return (<span>{localGame.xNext ? 'X' : 'O'}'s turn!</span>)
	}

	/**
	 * Create list of history moves
	 */
	getMoves() {
		const localGame = this.getLocalGame();
		const history = localGame.history;
		return history.map((step, move) => {
			const desc = move ? `Go to move #${move}` : `Start again`;
			return (
				<div key={move} className="infoRow">
					<button className="infoButton" onClick={() => this.backInTime(move)}>{desc}</button>
				</div>
			);
		});
	}

	/**
	 * Change size of game table
	 */
	changeGridSize(size) {
		if (!size || size > 20 || size < 3)
			return;
		const localGame = this.getLocalGame();
		if (!localGame.gameMove || window.confirm("Do you want to change table size? This will restart current game and all it's progress will be lost!")) {
			this.updateState({
				gameState: GameManager.createGame(size),
				gameMove: 0,
				size: size,
				xNext: true,
				winner: null,
				highlight: GameManager.createGame(size),
				history: []
			});
		}
	}

	/**
	 * Load game from server
	 */
	loadGame(game) {
		game = GameManager.duplicate(game);
		this.updateState({
			gameState: JSON.parse(game.gameState),
			gameMove: game.gameMove,
			size: game.size,
			xNext: game.xNext,
			winner: game.winner,
			highlight: JSON.parse(game.highlight),
			history: JSON.parse(game.history)
		});
	}

	render() {
		const localGame = this.getLocalGame();
		return (
			<div className="game">
				<div className="gameSettings">
					<Settings onClick={(size) => this.changeGridSize(size)} />
				</div>
				<div className="gameFlex">
					<div className="gameHistory">
						<div className="status">
							<span>History of games</span>
						</div>
						<History
							load={(game) => this.loadGame(game)}
						/>
					</div>
					<div className={`gameBoard ${localGame.size > 10 ? 'small' : ''} ${localGame.winner ? 'disabled' : ''}`}>
						<div className="status">{this.getStatus()}</div>
						<Grid
							highlight={localGame.highlight}
							size={localGame.size}
							tiles={localGame.gameState}
							onClick={(i, j) => this.handleClick(i, j)}
						/>
					</div>
					<div className="gameInfo">
						<div className="status">
							<span>Move history</span>
						</div>
						<div className="info">{this.getMoves()}</div>
					</div>
				</div>
			</div>
		);
	}

}

export default compose(
	graphql(localGameQuery, { name: 'localGameQuery' }),
	graphql(updateLocalGameMutation, { name: 'updateLocalGameMutation' }),
	graphql(createGameMutation, { name: 'createGameMutation' })
)(Game);