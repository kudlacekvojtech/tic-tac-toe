//! React
import React, { Component } from 'react';
//! Libs
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache, HttpLink, ApolloLink } from 'apollo-boost';
import { withClientState } from 'apollo-link-state';
//! Services
import { GameManager } from './services/gameManager';
//! Components
import Game from './components/game';
//! Queries
import { localGameQuery } from './queries/queries';

const cache = new InMemoryCache();

const defaultState = {
	localGame: {
		__typename: 'Game',
		gameState: GameManager.createGame(3),
		gameMove: 0,
		size: 3,
		xNext: true,
		winner: null,
		highlight: GameManager.createGame(3, false),
		history: []
	}
};

const stateLink = withClientState({
	cache,
	defaults: defaultState,
	resolvers: {
		Mutation: {
			updateLocalGame: (_, { data }, { cache }) => {
				let currentState = cache.readQuery({ query: localGameQuery });
				let newState = {
					...currentState,
					localGame: {
						...currentState.localGame,
						...data
					}
				}
				cache.writeData({ query: localGameQuery, data: newState });
				return newState.localGame;
			}
		}
	}
});

const client = new ApolloClient({
	cache,
	link: ApolloLink.from([
		stateLink,
		new HttpLink({
			uri: 'http://localhost:4000'
		})
	])
});

class App extends Component {

	render() {
		return (
			<ApolloProvider client={client}>
				<Game />
			</ApolloProvider>
		)
	}

}

export default App;